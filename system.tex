\section{System Model}
\label{sec:system-model}
%
In this section, the modeling of the network layer (NET), the medium access layer (MAC), the physical layer (PHY) and topology control is presented. 
Firstly, the NET is discussed, where nodes and their respective links are represented in a graph. 
Secondly, a collision-free scheduler at the MAC is presented. 
Thirdly, the concept of virtualization is illustrated, which enables the consideration of unicast and broadcast transmissions in a unified graph. 
Fourthly, topology control is introduced. 
Throughout this paper, the wireless multihop network consists of half-duplex nodes equipped with single omnidirectional antennas. 
Furthermore, it is assumed that all nodes in the network are battery powered. 
%
\subsection{Network Layer}
%
At the NET, the wireless multihop network is modeled as a graph $G$ in a two-dimensional space. 
The graph $G$ consists of a set of vertices $V$ and a set of edges $E$, where $V$ represents the nodes and $E$ represents the links in the network.
An edge $e_i$ is present between node $v_i$ and $v_j$ only if the two nodes are directly connected. 
This is exemplified in Fig. \ref{fig:graph}, where the weights at the edges represent the link capacity between two nodes. 
It is assumed that the link capacity is a function of the distance. 
Hence, the longer the distance between two nodes, the smaller the link capacity between them. 
Furthermore, it is assumed that the link capacity of the forward edge from $v_i$ and $v_j$ and the backward edge from $v_j$ to $v_i$ are symmetric.

We consider two approaches for solving the routing problem at the NET.
The first approach is tree routing. Here, the source node determines the shortest path to its destination nodes and relay nodes store and forward packets. 
%
The second approach is NC, where the relay nodes are capable of combining several incoming packets into a single outgoing packet. 
Destination nodes receive several coded and uncoded packets over multiple paths, which enables them to decode the received coded packets. 
%
\subsection{MAC Layer}
%
In wireless multihop networks, wireless communication needs to be coordinated to avoid collisions between concurrent transmissions. 
A collision occurs either when a node is transmitting and receiving at the same time or when a node is receiving multiple transmissions at the same time. 
Therefore, a collision-free scheduler is utilized at the MAC. 
The scheduler splits the complete network graph $G = (V, E)$ into $P$ subgraphs $G_p \subseteq G$. 
Furthermore, each subgraph $G_p$ contains a subset of vertices $V_p \subseteq V$ and edges $E_p \subseteq E$. 
Each subgraph $G_p$ represents a scheduling decision, which only contains vertices and edges that do not collide with each other. 
The scheduler activates the subgraphs by allocating resources to each of the subgraphs such that the overall rate achieved is maximized. 
%
\begin{figure}[t]
\centering
				\vspace{6mm}
        \includegraphics[ width = 0.5\columnwidth]{Figures/virtualization_2}
        %\vspace{-1mm}
        \caption{Node Virtualization: Introducing a virtual node $N^v$ and three virtual links to represent the broadcast transmission from $N$ to $D_1$ and $D_2$.}
        \label{fig:virtualization}
      % \vspace{-5mm}
\end{figure}
%
\subsection{Physical Layer}
%
The wireless multihop network supports broadcast and unicast transmissions at the PHY. 
In a unicast transmission, the packet is forwarded from a single sender to a single receiver. 
Hence, only one outgoing link is activated during a unicast transmission. 
In a broadcast transmission, a sender forwards the packet to all neighboring nodes simultaneously. 
In order to consider unicast and broadcast transmissions, both need to be represented in the graph $G$. 
This can be done by applying the concept of virtualization, cf. \cite{mousie}. 
Virtualization extends a given graph $G$ by adding virtual nodes and virtual edges. 
This concept is illustrated in Fig.~\ref{fig:virtualization}, where for node $N_4$ a virtual node $N_4^v$ is added. 
The virtual node $N_4^v$ has an incoming edge from the physical node $N_4$ and outgoing edges to all the receivers of $N_4$, namely $D_1$ and $D_2$. 
The link capacities of the virtual links are set to the minimum of the original outgoing links between the transmitting node $N_4$ and its receivers $D_1$ and $D_2$. 
In general, virtualization is applied for every node that has at least two outgoing links. 
%
\subsection{Topology Control}
%
One way to reduce energy consumption in wireless multihop networks is to apply TC \cite{TC-energy}. 
Throughout this paper, the TC algorithm kTC is considered \cite{ktc}. 
The main idea of kTC is to simplify a network by removing the longest edges from triangles in the network topology. 
On one hand, this deactivates links with low capacities, which may increase the length of the overall path. 
On the other hand, this reduces the number of available multiple paths in the network, which reduces the opportunities to utilize NC. 