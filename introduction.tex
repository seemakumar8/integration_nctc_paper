\section{Introduction}
\label{sec:Introduction}

In the upcoming Internet of Things, humans are surrounded by a multitude of devices with wireless communication capabilities~\cite{Atzori2010}.
Forming wireless multihop networks, these devices cooperate in order to achieve common goals, like delivering multimedia content~\cite{zhou2013energy} or participatory sensing~\cite{Meurisch2013}.

A plethora of research papers aim at optimizing wireless multihop networks for specific application scenarios.
Two optimization metrics dominate this existing research:
(i)~throughput and (ii)~energy consumption.

(i)~A high throughput is important for a variety of applications because of a high amount of data being generated.
Such applications include, for example, video streaming and data analytics.
A prominent approach for improving the throughput of wireless multihop networks is to use network coding~(NC)~\cite{networkcoding}.
Executing NC, relay nodes combine incoming packets into single or multiple outgoing packets.
Thereby, NC increases the flow of information through the network.

(ii)~A low energy consumption is a core requirement for a sufficient lifetime of wireless multihop networks because the devices have no external power supply.
An approach often proposed for increasing network lifetime is topology control (TC)~\cite{Wang2008}.
TC removes inefficient wireless links from the network topology to enforce routing over energy-efficient wireless links.

This paper investigates the following research question: How can NC and TC be combined to address both optimization metrics, throughput and energy consumption, in a reasonable way?
To answer the aforementioned question, we study the effect of NC and TC on wireless multihop networks individually and together. 
Extending an existing network simulation framework~\cite{mousie}, we evaluate the integration of NC and TC for various configurations and provide a detailed performance analysis.
It turns out that combining NC and TC is challenging because TC removes redundancy from the topology, while NC exploits redundancy in the topology.

Based on our performance analysis, we conduct a case study centered around data sharing among users.
This case study reflects a typical real-world scenario such as a conference meeting.
%
We propose NECTA (\textbf{NE}twork \textbf{C}oding and \textbf{T}opology \textbf{C}ontrol based \textbf{A}lgorithm) to optimize throughput and energy consumption based on the remaining energy level of the devices in the network:
When the energy level is high, optimizing throughput is the major concern.
As soon as the energy level drops below a configurable threshold, NECTA optimizes energy consumption rather than throughput because increasing network lifetime becomes the primary goal. This enables the devices to function, without turning off, e.g., until an alternate power source is available. Providing high system throughput with reduced energy consumption has been thoroughly studied and shows that there is a trade-off between throughput maximization and energy minimization \cite{trade-offs-2}. In order to take into account this trade-off, NECTA adopts a switching approach. 
%
Conducting a simulation study, we show that NECTA impressively increases the number of packets that can be successfully delivered before the network disconnects due to battery drain of devices. The case study provides a first step to show how integration of NC and TC can be benefited in real world scenarios.

In summary, this paper provides the following contributions:
\begin{itemize}
\item We analyze the performance of NC, TC and the integration of both in terms of throughput and energy consumption.
\item We propose NECTA, combining NC and TC in a practical data sharing scenario.
\item We evaluate NECTA in a simulation study.
\end{itemize}

The remainder of this paper is structured as follows: Sec.~\ref{sec:related-work} presents the related work on integrating NC and TC. 
Sec.~\ref{sec:system-model} presents our system model.
Sec.~\ref{sec:nctc} gives implementation details for our framework extension.
%Sec. \ref{sec:nctc} explains the details of integration of NC and TC, also the details of framework implemented that supports NC and TC. 
The performance analysis is conducted in Sec.~\ref{sec:evaluation}. 
In Sec.~\ref{sec:necta}, we present the case-study and the NECTA algorithm, followed by the conclusion in Sec.~\ref{sec:conclusion}.




%%
%Network Coding (NC) \cite{networkcoding} is a technique where the relay nodes combine the incoming packets into single or multiple outgoing packets. 
%The main benefit of NC is that it increases the rate of packets traversing the network, thereby increasing the system throughput. 
%However, in order to decode the coded packet at the destination node, NC requires the coded packet and uncoded original packet which are usually obtained from two different paths. 
%Though NC improves the system throughput to greater extent, the drawback is that NC consumes more energy due to the additional computation involved and additional transmissions. 
%Paramanathan et al. \cite{energy3} show the overall energy consumed by a system using NC is higher compared to a system without NC. \\
%%
%Topology Control (TC) is a technique that simplifies the network to utilize the resources efficiently. 
%There are algorithms that optimize the energy usage using TC \cite{TC-energy}. 
%NC and TC can work together to achieve both system throughput and energy consumption. 
%But, it is important to note that the two techniques have contradicting requirements. 
%NC requires data packets to be sent in two independent paths as coded and uncoded messages \cite{mousie}. 
%Whereas, TC simplifies the network which reduces the multiple paths available between the nodes. \\
%%
%The throughput of the system depends on the capacity of each link in the network. 
%If the TC algorithm removes edges with low capacities from the network, it enables NC to utilize edges with high capacities for forwarding. 
%Hence, the integration can degrade the system performance further, or it can improve to a greater extent. 
%To the best of our knowledge, the problem of investigating the impact of NC and TC on each other remains open. 
%In this paper, we study the effect of NC and TC individually and together on wireless ad hoc networks. 
%We evaluate the integration of NC and TC on various use-cases and provide a detailed analysis on the performance. \\
%%
%In order to utilize the efforts of this work in real world deployments, we perform a case-study which shows an implementation of NC-TC integration. 
%For the case-study we consider the scenarios of conferences, meeting rooms, etc where data needs to be shared among users. 
%A wireless ad hoc network is formed among the battery powered devices, hence energy conservation becomes critical. 
%Providing high system throughput with reduced energy consumption in wireless and ad hoc networks has been researched and analyzed extensively. 
%Researchers show a trade-off between system throughput and energy consumption \cite{trade-offs-2}. \\
%%
%In this paper, we propose a simple algorithm to maximising system throughput, based on the availability of energy in the devices that form the ad hoc network. 
%When the energy level drops below a certain threshold, the energy utilisation on the device is optimised. 
%This enables the devices to function, without turning off, until an alternate power source is available. 
%The network lifetime will also be extended by extending the device's lifetime. 
%As part of the case study, the algorithm is proposed that uses NC and TC to provide higher throughput and also extend network lifetime during low power. 
%The case study acts as the first step to show how the integration of NC and TC can be benefited. \\
%%
%The contributions of this paper are: 
%%
%\begin{itemize}
%\item A framework called `NcTc Framework', that can be configured to use NC or TC or both together for a given network. 
%\item Study the behavior of NC, TC and the integration of the both in terms of system throughput and overall energy consumption. 
%%To investigate the behavior, a given network is evaluated under four circumstances. A network with traditional store and forward routing, NC, TC and integration of NC and TC.
%\item Based on the outcome of the investigation performed, a case study is performed to show how the benefits of integrating NC and TC can be utilized in practical deployments.
%\item As part of the case-study, an algorithm called {\bf NECTA: NEtwork Coding and Topology Control based Algorithm for improved performance}, is proposed that combines the two approaches to achieve higher throughput and extend the lifetime of the network by optimizing the energy consumption.
%\item NECTA is evaluated based on the number of packets that can be successfully transmitted to the destination before the network becomes disconnected due to node failure. 
%The node failure here refers to failure due to battery drain.
%\end{itemize}
%%
%The outline of the paper is as follows: Sec. \ref{sec:related-work} presents the previous efforts in integrating network coding and topology control. 
%Sec. \ref{sec:system-model} discusses the system model considered for the study. 
%Sec. \ref{sec:nctc} explains the details of integration of NC and TC, also the details of framework implemented that supports NC and TC. 
%The evaluation results and detailed analysis are presented in Sec. \ref{sec:evaluation}. 
%In Sec. \ref{sec:necta}, the case-study is presented which also describes the NECTA algorithm, followed by conclusion in Sec. \ref{sec:conclusion}.



\begin{figure}[t]
\centering
		\vspace{6mm}
        \includegraphics[width=0.5\columnwidth]{Figures/sysmodel}
              %  \vspace{-1mm}
        \caption{Wireless multihop network modeled as a graph.}
        \label{fig:graph}
\end{figure}
  \hspace{3mm}
