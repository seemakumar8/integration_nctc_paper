\section{Performance Evaluation}
\label{sec:evaluation}
%
For the evaluation, we generate random wireless multihop networks with the simulation parameters specified in Table \ref{table:simulation-parameters}. 
The network consists of 25 nodes with one source and four destination nodes, and the remaining 20 nodes act as relay nodes. 
The nodes are placed randomly within a given area based on a uniform distribution. 
The transmission range of a node is between 1 m and 5 m. 
The simulations are performed in Matlab and the presented results are averaged over 100 random networks. 
%
\begin{table}[b]
\centering
\begin{tabular}{|l|l|ll}
\cline{1-2}
\textbf{Parameter}         & \multicolumn{1}{c|}{\textbf{Value}} & \multicolumn{1}{c}{\textbf{}} &  \\ \cline{1-2}
Number of random networks  & 100                                 &                               &  \\ \cline{1-2}
Distance between two nodes & 1 m - 5 m                           &                               &  \\ \cline{1-2}
Area of the network        & 10 m x 5 m                           &                               &  \\ \cline{1-2}
Tx Power				   &  0 dBm								 &							     & \\ \cline{1-2}
\end{tabular}
\vspace{1mm}
\caption{Simulation parameters.}
\label{table:simulation-parameters}
\vspace{-2mm}
\end{table}
%
\subsection{Performance Metrics}
\label{sec:Eval-metrics}
%
The system throughput and the overall energy consumed by the system are chosen as performance metrics, which are obtained as follows. \\
%
The system throughput is the sum of rates achieved by each destination. 
The rate at each destination is determined by the maximum flow from the source to the destination. \\
%
An energy consumption model is introduced in order to compute the energy consumption in the system. 
The energy required to transmit data is calculated as follows: 
%
\begin{equation}
E_{\text{tot}} = (P_1 \cdot l_1 \cdot d_1) + (P_2 \cdot l_2 \cdot d_2) + \cdots + (P_n \cdot l_n \cdot d_n), 
\end{equation}
%
where $E_{\text{tot}}$ is the overall energy consumed in the system, $P_i$ is the transmit power, $l_i$ indicates if link $i$ is activated for a transmission and $d_i$ expresses the duration link $i$ is active. 
%
\begin{figure}[t]
\centering
\includegraphics[width=\columnwidth]{plots/2016_icc_sim_variants_2.eps}
        \caption{System throughput and energy consumption for the four configurations Tree, NC, TC+Tree and TC+NC.}
        \vspace{-5mm}
        \label{fig:plot2}
\end{figure}
%
\subsection{Possible Configurations}
\label{sec:Eval-cases}
%
In order to analyze the adaptability of the NcTc framework, we identify four configurations: 
\begin{enumerate}
\item Tree: TC is disabled, and only tree routing is considered. 
\item NC: TC is disabled, but tree routing and NC at the NET are considered. 
\item TC+Tree: TC and tree routing is considered. 
\item TC+NC: TC, tree routing and NC are considered.
\end{enumerate}
%
\subsection{Simulation Results}
\label{sec:Simulation-Results}
The first simulation results are shown in Fig. \ref{fig:plot2}, where the top figure shows the achieved system throughput for the four configurations and the bottom figure shows the total energy consumed in the system for the four configurations. 
Moreover, Fig.~\ref{fig:plot2} shows the performance when only broadcast transmissions, only unicast transmissions or when both unicast and broadcast transmissions are utilized at the PHY. \\
Fig.~\ref{fig:plot2} shows that the two configurations NC and TC+NC achieve high average system throughput, while the two configurations Tree and TC+Tree consume less energy. 
Furthermore, the results show that utilizing only broadcast transmissions leads to the lowest system throughput and to the highest energy consumption for all four configurations. 
However, it should be noted that TC reduces the energy consumption with almost no degradation in system throughput when only broadcast transmissions are used at the PHY. \\
In comparison, applying unicast transmissions at the PHY provides the lowest energy consumption and achieves high system throughput for all four configurations. 
%
The combination of NC at the NET, unicast and broadcast transmissions at the PHY and TC at the intermediate layer provides a very good trade-off between high system throughput and low energy consumption. 
The configuration TC+NC in combination with unicast and broadcast transmission consumes 44\% less energy in comparison to the configuration NC at the cost of a reduction of the system throughput by only 2\%. \\
%
Fig. \ref{fig:plot2} shows that the overall energy consumption is reduced when TC is applied, regardless if tree or NC is utilized at the NET. 
Furthermore, the following two effects can be observed with TC with regards to unicast and broadcast transmissions at the PHY. \\
For unicast transmissions, the system throughput is reduced by 24\% for the configuration TC+Tree and by 8\% for the configuration TC+NC, while the energy consumption increases by 13\% for the configuration TC+Tree and by 30\% for the configuration TC+NC.
One reason for this effect is that potential short paths are eliminated, leading to longer paths with more hops and more nodes. 
Therefore, this may lead to reducing the system throughput and increasing the energy consumption of the network. 
On the other hand, for broadcast transmissions, the system throughput increases by 5\% for both configurations and the energy consumption decreases by 14\% for the configuration TC+Tree and by 24\% for the configuration TC+NC.
The reason is that the TC algorithm removes the longest edges of triangles, which are links with low capacity. 
Therefore, the broadcast capacity of each node in the network is increased, leading to higher rates and shorter transmission time. \\
%
Table \ref{table:comparison-25nodes} summarizes the system throughput and the total energy consumption for the various configurations and the different transmissions. 
The simulation results reveal that using NC at the NET and both unicast and broadcast transmissions at the PHY without TC should be chosen if maximizing the system throughput is the single goal. 
On the other hand, Tree at the NET and unicast transmission at the PHY without TC should be chosen if minimizing the overall energy consumption is the single goal. \\
%
However, if both system throughput and energy consumption are the goal, there is not an optimal choice. 
There are different possible configurations either utilizing NC at the NET, unicast transmissions at the PHY and not applying TC, or utilzing NC at the NET, unicast and broadcast transmission at the PHY and applying TC. 
Since the requirements of the network can change over time, a better approach is to switch between different configurations to fulfill the given requirements. \\
%
One possible way of switching between different configurations is given in the next section. 
%
\begin{table}[b]
\centering
\scalebox{0.9} {
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline
& \multicolumn{3}{c|}{\textbf{System Throughput (bits/s/Hz)}} & \multicolumn{3}{c|}{\textbf{Energy Consumption (mW$\cdot$s)}} \\
\hline
& \multicolumn{1}{c|}{\textbf{Broadcast}} & \multicolumn{1}{c|}{\textbf{Unicast}} & \multicolumn{1}{c|}{\textbf{Both}} & \multicolumn{1}{c|}{\textbf{Broadcast}} & \multicolumn{1}{c|}{\textbf{Unicast}} & \multicolumn{1}{c|}{\textbf{Both}} \\
\hline
\textbf{Tree} & 0.1217 & 0.2161 & 0.1724 & 0.2436 & 0.0962 & 0.1164 \\ 
\hline
\textbf{NC} & 0.3541 & 0.4324 & 0.4682 & 0.6679 & 0.1506 & 0.6048 \\ 
\hline
\textbf{TC+Tree} & 0.1284 & 0.1738 & 0.1698 & 0.2092 & 0.1088 & 0.1267 \\ 
\hline
\textbf{TC+NC} & 0.3708 & 0.4002 & 0.4597 & 0.5039 & 0.1961 & 0.3384 \\ 
\hline
\end{tabular}
}
%\vspace{0.1mm}
\caption{System throughput and energy consumption for the configuration Tree, NC, TC+Tree and TC+NC obtained from Fig~\ref{fig:plot2}.}
\label{table:comparison-25nodes}
%\vspace{2mm}
\end{table}
%
%It can be observed, that NC provides high throughput but consumes a large amount of energy. 
%This due to the fact, that NC combines packets and utilizes multiple paths to achieve a high system throughput, requiring more nodes that communicate with each other, which leads to a higher energy consumption. 
%In combination NC with TC, the overall energy consumption can be reduced by 50\%. \\
%%
%Furthermore, the results show that TC increases the system throughput with a low increase in energy consumption, when only broadcast transmissions are utilized at the PHY. 
%This is achieved, because TC removes edges with the lowest link capacity in a triangle which increases the overall link capacity in the system. 
%This results in improved broadcast transmissions and hence in a high system throughput. \\
%%
%\textcolor{red}{With TC, the longest edge forming the triangle is removed. 
%If this longest edge is used for transmission, removing this edge will add an additional hop for the transmission. 
%Hence, the energy consumed with TC is higher compared to without TC. 
%Though TC removes the link with least capacity, the number of hops used for transmission has a higher effect on energy consumption than the link capacity.} \\
%%
%\textcolor{red}{The broadcast link capacity is limited by the lowest link capacity and hence low throughput is achieved with broadcast transmissions. 
%On average 12.1 links were used with tree routing for transmission of a single unit of data in a network using broadcast transmissions. 
%Whereas, 15.2 links were activated in a network using unicast transmissions. 
%Though the number of hops used for broadcast transmissions are lower than for unicast transmissions, the energy consumption of broadcast transmissions is higher. 
%Since the transmission rate is limited lowest link capacity for broadcast transmissions, the transmission between two nodes takes longer. 
%Therefore, in the case of broadcast transmissions, the higher energy consumption is a the result of low link capacity and not due to the number of hops.} 
%